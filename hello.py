from flask import Flask,render_template
app = Flask(__name__)

# @app.route('/')
# def index():
#     return 'Hello this is the home page'

# @app.route('/hello')
@app.route('/')
def hello():
    return render_template('index.html')

@app.route('/about.html')
def about():
    return render_template('about.html')
# def hello():
#     return 'Hello, World!'
