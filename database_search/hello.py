#In terminal, $export FLASK_APP=hello.py $FLASK_ENV=development
from flask import Flask,render_template,request
from grand_search import *
app = Flask(__name__) #app is just a name

@app.route('/')
def hello():
    return render_template('index.html')

@app.route("/result", methods=['GET', 'POST'])
def result():
    if request.method == 'POST':
        # print(request, " ", request.form['query'])  # not catching errors, should add that
        movie = request.form['movie']
        info = request.form['info']
    result_list = grandsearch(movie,info)
    return render_template('result.html', title="result: "+movie+info, meta_description="awesome page", results=result_list)

@app.route('/about.html')
def about():
    return render_template('about.html')
