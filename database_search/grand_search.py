#Create dictionaries with movie as keys and each info as value
movie_people_time = open('movie.people_time.txt', 'r')
#Remove empty line in document, creat a list with each line as a string element
data_people_time = [line for line in movie_people_time.readlines() if line.strip()]
#for each line, put each line into list of seperate elements and append to dictionary
director = {} #str
actors = {} #str
production_year = {} #int
length = {} #int
for i in data_people_time[1:]:
    director[i.rstrip("\n").split("\t")[1]] = i.rstrip("\n").split("\t")[2]
    actors[i.rstrip("\n").split("\t")[1]] = i.rstrip("\n").split("\t")[3]
    production_year[i.rstrip("\n").split("\t")[1]] = int(i.rstrip("\n").split("\t")[4])
    length[i.rstrip("\n").split("\t")[1]] = int(i.rstrip("\n").split("\t")[5])

movie_desc = open ('movie.desc.txt', 'r')
data_desc = [line for line in movie_desc.readlines() if line.strip()]
categories = {} #list
brief = {} #str
for i in data_desc[1:]:
    categories[i.rstrip("\n").split("\t")[1]] = i.rstrip("\n").split("\t")[2].split(',') #make the value of categories as a list
    brief[i.rstrip("\n").split("\t")[1]] = i.rstrip("\n").split("\t")[3]

movie_rating = open('movie.ratings.txt','r') #there are blanks in this file for some elements
data_rating = [line for line in movie_rating.readlines() if line.strip()]
rating = {} #float
votes = {} #int
boxoffice = {} #float
score = {} #int
for i in data_rating[1:]:
    rating[i.rstrip("\n").split("\t")[1]] = float(i.rstrip("\n").split("\t")[2])
    votes[i.rstrip("\n").split("\t")[1]] = int(i.rstrip("\n").split("\t")[3])
    if i.rstrip("\n").split("\t")[4] != '': boxoffice[i.rstrip("\n").split("\t")[1]] = float(i.rstrip("\n").split("\t")[4])
    else:  boxoffice[i.rstrip("\n").split("\t")[1]] = float('nan')
    if i.rstrip("\n").split("\t")[5] != '': score[i.rstrip("\n").split("\t")[1]] = int(i.rstrip("\n").split("\t")[5])
    else: score[i.rstrip("\n").split("\t")[1]] = float('nan')

def grandsearch(movie,info):
    """Return info of the corresponding movie
    this gets a bit tedious as input from html is str,so that we need to convert info to a dict that call the corresponding list
    """
    infodict = {'director':director,'actors':actors,'production_year':production_year,'length':length,'categories':categories,'brief':brief,'rating':rating,'votes':votes,'boxoffice':boxoffice,'score':score}
    if infodict[info].get(movie,0) == 0: return('Movie outside our data base')
    return infodict[info][movie]

# following function are just for other search possibilities
# def namesearch(name):
#     """Return movies corresponding to name of actors or director"""
#     movielist = []
#     for x in actors:
#         if name in actors[x]:
#             movielist.append(x)
#     for y in director:
#         if name in director[y]:
#             movielist.append(y)
#     return movielist
# def ratesearch(rate):
#     """Return movies with rate better than input"""
#     movielist = []
#     for movie in rating:
#         if rating[movie] >= rate:
#             movielist.append(movie)
#     return movielist

# if __name__ == '__main__':
#     print(ratesearch(8))
