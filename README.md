# 12 Oct Friday Challenge
* This challenge is mainly about creating a google-like search page for front-end practise and back-end practise with python flask.
* Final version is in the [database_search](https://gitlab.com/sallyyklai/12-oct-challenge/tree/master/database_search) folder which run a server to allow filtering of films in the txt files according to their properties.
* Most of the pictures are put online in order for the server to work, link can be found in the html file under the img handle.
